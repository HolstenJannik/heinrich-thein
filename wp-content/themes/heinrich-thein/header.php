<html>
  <head>
      <title>Heinrich Thein</title>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
      <?php wp_head(); ?>
  </head>
  <body>
  <div class="mainHeader">
      <div class="header">
          <a class="homeLink" href="<?php $link = get_field('startseite_link'); echo $link['url']; ?>"><span class="glyphicon glyphicon-home"></span></a>
          <img class="background" src="<?php echo get_field('header_bild'); ?>" />
          <?php if (have_rows('portrait')): ?>
              <?php while (have_rows('portrait')): the_row();
                  $image = get_sub_field('portrait_img');
                  $title = get_sub_field('portrait_title');
                  $text  = get_sub_field('portrait_text');
                  ?>
                  <img class="portrait img-responsive" src="<?php echo $image; ?>" />
                  <div class="headline">
                      <h1><?php echo $title; ?></h1>
                  </div>
                  <div class="imgDes">
                      <p><?php echo $text; ?></p>
                  </div>
              <?php endwhile; ?>
          <?php endif; ?>
      </div>
      <div class="spaceMaker"></div>
  </div>