<?php get_header(); ?>

<div class="content">
	<div class="history" id="vita">
		<div class="row">
			<div class="col-md-6 leftHistory">
				<?php if (have_rows('section_left')): ?>
					<?php while (have_rows('section_left')): the_row();
						$headline = get_sub_field('headline');
						$name     = get_sub_field('name');
						$text1    = get_sub_field('text1');
						$text2    = get_sub_field('text2');
						$text3    = get_sub_field('text3');
						$text4    = get_sub_field('text4');
						$text5    = get_sub_field('text5');
						$text6    = get_sub_field('text6');
					?>
						<h1><?php echo $headline; ?></h1>
						<h4><?php echo $name; ?></h4>
						<p><?php echo $text1; ?></p>
						<p><?php echo $text2; ?></p>
						<p><?php echo $text3; ?></p>
						<p><?php echo $text4; ?></p>
						<p><?php echo $text5; ?></p>
						<p><?php echo $text6; ?></p>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<div class="col-md-6 rightHistory normalHistory">
                <div id="scroller pull-left">
                    <div id="Scrollbar-Container">
                        <div class="Scrollbar-Track">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/scrollbar_handle.gif" class="Scrollbar-Handle" />
                        </div>
                    </div>
                    <div class="Container">
                        <div id="Scroller-1">
                            <div class="Scroller-Container">
                                <table>
		                            <?php if (have_rows('section_right')):  ?>
			                            <?php while (have_rows('section_right')): the_row();
				                            $year     = get_sub_field('year');
				                            $yearText = get_sub_field('year_text');
				                            ?>
                                            <tr>
                                                <td class="time"><?php echo $year; ?></td>
                                                <td><?php echo $yearText; ?></td>
                                            </tr>
			                            <?php endwhile; ?>
		                            <?php endif; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <div class="col-md-6 rightHistory mobileHistory">
                <div class="line"></div>
                <table>
		            <?php if (have_rows('section_right')):  ?>
			            <?php while (have_rows('section_right')): the_row();
				            $year     = get_sub_field('year');
				            $yearText = get_sub_field('year_text');
				            ?>
                            <tr>
                                <td class="time"><?php echo $year; ?></td>
                                <td><?php echo $yearText; ?></td>
                            </tr>
			            <?php endwhile; ?>
		            <?php endif; ?>
                </table>
            </div>
		</div>
	</div>
	<div class="explanation" id="komponieren">
		<div class="inner">
			<h3><?php _e(get_field('headline'), false); ?></h3>
			<div class="line"></div>
			<div class="text">
				<p><?php echo get_field('main_text'); ?></p>
			</div>
			<div class="row bottomStuff">
				<div class="col-md-5 theinGrey">
					<img class="img-responsive" src="<?php echo get_field('section_img'); ?>" />
				</div>
				<div class="col-md-4 smallText">
					<h5 style="padding-bottom: 10px"><?php echo get_field('headline_bottom'); ?></h5>
					<h5><?php echo get_field('sub_headline_bottom') ?></h5>
					<p><?php echo get_field('small_text'); ?></p>
					<h5><?php echo get_field('third_headline_bottom'); ?></h5>
					<p><?php echo get_field('small_text_2'); ?></p>
				</div>
				<div class="col-md-3 pull-down">
					<img class="hidden-sm hidden-xs" src="<?php echo get_field('icon'); ?>" />
				</div>
			</div>
			<div class="specialHeader hidden-sm hidden-xs">
				<h1><?php echo get_field('special_header'); ?></h1>
			</div>
		</div>
	</div>
	<div class="works" id="works">
		<div class="row">
            <div class="normalWorks">
                <div class="col-md-6 leftWorks">
                    <h1><?php echo get_field('works_headline'); ?></h1>
                    <?php if(have_rows('works')): $typeId = 1 ?>
                        <?php while(have_rows('works')): the_row();
                            $worksType = get_sub_field('type');
                        ?>
                            <a id="field<?php echo $typeId; ?>"><span></span> <?php echo $worksType; ?></a>
                        <?php $typeId++; endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-6 rightWorks">
		            <?php if (have_rows('works')): $textId = 2; ?>
			            <?php while (have_rows('works')): the_row();
				            $worksText  = get_sub_field('works_text');
				            $type       = get_sub_field('type');
				            $typeString = str_replace(' ', '_', strtolower($type));
                        ?>
                            <div class="worksText" id="<?php echo $typeString; ?>">
                                <div id="scroller pull-left">
                                    <div id="Scrollbar-Container-<?php echo $textId; ?>">
                                        <div class="Scrollbar-Track">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/scrollbar_handle.gif" class="Scrollbar-Handle" />
                                        </div>
                                    </div>
                                    <div class="Container">
                                        <div id="Scroller-<?php echo $textId; ?>">
                                            <div class="Scroller-Container-<?php echo $textId; ?>">
                                                <p><?php echo $worksText; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $textId++; endwhile; ?>
		            <?php endif; ?>
                </div>
            </div>

            <div class="col-md-6 mobileWorks">
                <h1><?php echo get_field('works_headline'); ?></h1>
                <div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php if (have_rows('works')): $worksId = 1; ?>
                            <?php while (have_rows('works')): the_row();
                                $typeMobile      = get_sub_field('type');
                                $worksTextMobile = get_sub_field('works_text');
                            ?>
                                <div class="panel">
                                    <a id="openCollapse" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $worksId; ?>" aria-expanded="true" aria-controls"collapse<?php echo $worksId; ?>">
                                        <div class="panel-heading" role="tab" id="überschrift<?php echo $worksId; ?>">
                                            <h4 class="panel-title">
                                                <?php echo $typeMobile; ?>
                                                <span class="iconUp glyphicon glyphicon-triangle-top"></span>
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="collapse<?php echo $worksId; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="überschrift<?php echo $worksId; ?>">
                                        <div class="panel-body">
                                            <?php echo $worksTextMobile; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php $worksId++; endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div class="publishers" id="eigenverlag">
		<h3><?php echo get_field('publishers_headline'); ?></h3>
		<div class="line"></div>
		<div class="row specialRow">
			<?php if (have_rows('all_cover')): $iconId = 1; ?>
				<?php while (have_rows('all_cover')): the_row();
					$coverImage  = get_sub_field('cover_img');
					$description = get_sub_field('cover_beschreibung');
				?>
					<div class="col-sm-2 col-xs-6 coverElement element">
						<img id="icon-<?php echo $iconId; ?>" class="<?php if ($iconId % 2 != 0): ?>rightImg <?php else: ?>leftImg <?php endif; ?>coverImage img-responsive" src="<?php echo $coverImage; ?>" />
					</div>
                    <!--<div>
                        // Delete later
                        <img class="img-responsive showImage icon-<?php /*echo $iconId; */?>" src="<?php /*echo $coverImage; */?>" />
                    </div>-->

                    <div class="coverOverlay icon-<?php echo $iconId; ?> <?php empty($description) ? print 'noBorder' : print null; ?>">
                        <!--<span class="closeCover"></span>-->
                        <img class="topCoverImage" src="<?php echo $coverImage; ?>" />
                        <div class="description">
                            <?php echo $description; ?>
                        </div>
                    </div>

				<?php $iconId++; endwhile; ?>
			<?php endif; ?>
		</div>
		<p><?php echo get_field('sub_text'); ?></p>
        <a href="<?php echo get_field('order_button'); ?>?subject=Bestellen">
            <button>Bestellen</button>
		</a>
	</div>
	<div class="passion" id="passion">
		<div class="row topRow">
            <!-- // Desktop headline -->
			<div class="col-sm-6 images hidden-xs">
				<img src="<?php echo get_field('passion_image'); ?>" />
			</div>
			<div class="col-sm-6 headline hidden-xs">
				<h1><?php echo get_field('passion_headline'); ?></h1>
			</div>

            <!-- // Mobile headline -->
			<div class="col-sm-6 hidden-md hidden-lg hidden-sm">
				<h1><?php echo get_field('passion_headline'); ?></h1>
			</div>
			<div class="col-xs-6 hidden-md hidden-lg hidden-sm imageRight images">
				<img class="img-responsive" src="<?php echo get_field('passion_image'); ?>" />
			</div>
            <div class="col-xs-6 col-xs-offset-6 hidden-md hidden-lg hidden-sm imageLeft images">
                <img class="img-responsive" src="<?php echo get_field('static_image'); ?>" />
            </div>
		</div>
		<div class="row bottomRow hidden-xs">
            <div class="col-sm-6 images" id="slideshow1">
                <?php
                    $images = get_field('slide_show_1');
                    $id     = 1;

                    if ($images) :
                    foreach ($images as $image) :
                ?>
                    <img id="id-<?php echo $id; ?>" class="img-responsive" src="<?php echo $image['sizes']['medium_large']; ?>" />
                <?php
                    $id++;
                    endforeach;
                    endif;
                ?>
            </div>
            <div class="col-sm-3 images">
                <img class="img-responsive" src="<?php echo get_field('static_image'); ?>" />
            </div>
            <div class="col-sm-3 images" id="slideshow2">
                <?php
                    $images2 = get_field('slide_show_2');
                    $secondId = 1;

                    if ($images2) :
                    foreach ($images2 as $image2) :
                ?>
                    <img id="secondId-<?php echo $secondId; ?>" src="<?php echo $image2['sizes']['medium_large']; ?>" />
                <?php
                    $secondId++;
                    endforeach;
                    endif;
                ?>
            </div>
		</div>
	</div>
</div>

<?php get_footer(); ?>