<div class="footer">
    <div class="contentForm">
        <h3>Kontakt</h3>
        <div class="line"></div>
        <div class="fontStyles">
		    <?php /*echo do_shortcode('[contact-form-7 id="172" title="Contact Form"]'); */?>
            <div>
                <p>Sie erreichen mich unter:</p>
                <table>
                    <tr>
                        <td class="address" colspan="2">
                            Heinrich Thein<br />
                            Habichthorster Str. 2 A<br />
                            27721 Ritterhude-Stendorf
                        </td>
                    </tr>
                    <tr>
                        <td>Telefon:&nbsp;&nbsp;</td>
                        <td>0171 45 31 811</td>
                    </tr>
                    <tr>
                        <td>E-Mail:&nbsp;&nbsp;</td>
                        <td><a href="mailto:thein.heinrich@gmail.com?subject=Anfrage">thein.heinrich@gmail.com</a></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row footerLinks">
            <?php
                $linksUrl       = get_field('links_url');
                $impressumUrl   = get_field('impressum_url');
                $datenschutzUrl = get_field('datenschutz_url');
            ?>
            <div class="col-sm-1 col-sm-offset-4 linkLeft">
                <a href="<?php echo $linksUrl['url']; ?>"><?php echo $linksUrl['title']; ?></a>
            </div>
            <div class="col-sm-2">
                <a href="<?php echo $impressumUrl['url']; ?>"><?php echo $impressumUrl['title']; ?></a>
            </div>
            <div class="col-sm-1 linkRight">
                <a href="<?php echo $datenschutzUrl['url']; ?>"><?php echo $datenschutzUrl['title']; ?></a>
            </div>
        </div>
    </div>
</div>
<div class="blackout"></div>

<?php wp_footer(); ?>
</body>
</html>