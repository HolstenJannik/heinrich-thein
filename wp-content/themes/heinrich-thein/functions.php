<?php

// Add css files
function register_styles() {
	wp_register_style('style-css', get_template_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'register_styles');

// Add javascript files
function register_scripts() {
	wp_enqueue_script('scrollbar-js', get_template_directory_uri() . '/scripts/jsScrollbar.js', array(), '1.0.0', false);
	wp_enqueue_script('scroller-js', get_template_directory_uri() . '/scripts/jsScroller.js', array(), '1.0.0', false );
	wp_enqueue_script('main-js', get_template_directory_uri() . '/scripts/main.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'register_scripts');

// Add jQuery scripts
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
	wp_deregister_script('jquery');
	wp_register_script('jquery',  get_template_directory_uri() ."/scripts/jquery.js", array(), null, true);
	wp_enqueue_script('jquery');
}

// Page not found redirect
function redirect_page_not_found() {
	if (is_404()) {
		wp_redirect(home_url(), 301);
		die;
	}
}
add_action('wp', 'redirect_page_not_found', 1);

function theme_add_bootstrap() {
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array(), '3.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'theme_add_bootstrap' );