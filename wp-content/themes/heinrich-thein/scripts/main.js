$(document).ready( function () {

  /* Start - text-changer */
  var literarische_werke = $('#literarische_werke');
  var literarische_projekte = $('#literarische_projekte');
  var musikalische_werke = $('#musikalische_werke');
  var musikalische_projekte = $('#musikalische_projekte');
  var musikinstrumentenbau = $('#musikinstrumentenbau');
  var projekte_instrumentenbau = $('#projekte_instrumentenbau');

  $('.worksText').hide();
  literarische_werke.show();
  $('#field1').css('font-size', '19px');
  $('#field1 span').addClass('glyphicon glyphicon-menu-right');

  // Scroll 2

  if (document.getElementById("Scroller-2") !== null) {
      var scroller2  = null;
      var scrollbar2 = null;

      scroller2  = new jsScroller(document.getElementById("Scroller-2"), 400, 200, "Scroller-Container-2");
      scrollbar2 = new jsScrollbar(document.getElementById("Scrollbar-Container-2"), scroller2, false);

      $(window).on('load resize', function() {
          scroller2  = new jsScroller(document.getElementById("Scroller-2"), 400, 200, "Scroller-Container-2");
          scrollbar2 = new jsScrollbar(document.getElementById("Scrollbar-Container-2"), scroller2, false);
      });
  }

  $('#field1').click(function () {
    literarische_werke.show();
    literarische_projekte.hide();
    musikalische_werke.hide();
    musikalische_projekte.hide();
    musikinstrumentenbau.hide();
    projekte_instrumentenbau.hide();
    $('#field1').css('font-size', '19px');
    $('#field2').css('font-size', '15px');
    $('#field3').css('font-size', '15px');
    $('#field4').css('font-size', '15px');
    $('#field5').css('font-size', '15px');
    $('#field6').css('font-size', '15px');
    $('#field1 span').addClass('glyphicon glyphicon-menu-right');
    $('#field2 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field3 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field4 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field5 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field6 span').removeClass('glyphicon glyphicon-menu-right');

    // Scroll 2
    var scroller2  = null;
    var scrollbar2 = null;

    scroller2  = new jsScroller(document.getElementById("Scroller-2"), 400, 200, "Scroller-Container-2");
    scrollbar2 = new jsScrollbar(document.getElementById("Scrollbar-Container-2"), scroller2, false);

    $(window).on('load resize', function() {
      scroller2  = new jsScroller(document.getElementById("Scroller-2"), 400, 200, "Scroller-Container-2");
      scrollbar2 = new jsScrollbar(document.getElementById("Scrollbar-Container-2"), scroller2, false);
    });
  });

  $('#field2').click(function () {
    literarische_werke.hide();
    literarische_projekte.show();
    musikalische_werke.hide();
    musikalische_projekte.hide();
    musikinstrumentenbau.hide();
    projekte_instrumentenbau.hide();
    $('#field1').css('font-size', '15px');
    $('#field2').css('font-size', '19px');
    $('#field3').css('font-size', '15px');
    $('#field4').css('font-size', '15px');
    $('#field5').css('font-size', '15px');
    $('#field6').css('font-size', '15px');
    $('#field1 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field2 span').addClass('glyphicon glyphicon-menu-right');
    $('#field3 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field4 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field5 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field6 span').removeClass('glyphicon glyphicon-menu-right');

    // Scroll 3
    var scroller3  = null;
    var scrollbar3 = null;

    scroller3  = new jsScroller(document.getElementById("Scroller-3"), 400, 200, "Scroller-Container-3");
    scrollbar3 = new jsScrollbar(document.getElementById("Scrollbar-Container-3"), scroller3, false);

    $(window).on('load resize', function() {
      scroller3  = new jsScroller(document.getElementById("Scroller-3"), 400, 200, "Scroller-Container-3");
      scrollbar3 = new jsScrollbar(document.getElementById("Scrollbar-Container-3"), scroller3, false);
    });
  });

  $('#field3').click(function () {
    literarische_werke.hide();
    literarische_projekte.hide();
    musikalische_werke.show();
    musikalische_projekte.hide();
    musikinstrumentenbau.hide();
    projekte_instrumentenbau.hide();
    $('#field1').css('font-size', '15px');
    $('#field2').css('font-size', '15px');
    $('#field3').css('font-size', '19px');
    $('#field4').css('font-size', '15px');
    $('#field5').css('font-size', '15px');
    $('#field6').css('font-size', '15px');
    $('#field1 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field2 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field3 span').addClass('glyphicon glyphicon-menu-right');
    $('#field4 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field5 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field6 span').removeClass('glyphicon glyphicon-menu-right');

    // Scroll 4
    var scroller4  = null;
    var scrollbar4 = null;

    scroller4  = new jsScroller(document.getElementById("Scroller-4"), 400, 200, "Scroller-Container-4");
    scrollbar4 = new jsScrollbar(document.getElementById("Scrollbar-Container-4"), scroller4, false);

    $(window).on('load resize', function() {
      scroller4  = new jsScroller(document.getElementById("Scroller-4"), 400, 200, "Scroller-Container-4");
      scrollbar4 = new jsScrollbar(document.getElementById("Scrollbar-Container-4"), scroller4, false);
    });
  });

  $('#field4').click(function () {
    literarische_werke.hide();
    literarische_projekte.hide();
    musikalische_werke.hide();
    musikalische_projekte.show();
    musikinstrumentenbau.hide();
    projekte_instrumentenbau.hide();
    $('#field1').css('font-size', '15px');
    $('#field2').css('font-size', '15px');
    $('#field3').css('font-size', '15px');
    $('#field4').css('font-size', '19px');
    $('#field5').css('font-size', '15px');
    $('#field6').css('font-size', '15px');
    $('#field1 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field2 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field3 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field4 span').addClass('glyphicon glyphicon-menu-right');
    $('#field5 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field6 span').removeClass('glyphicon glyphicon-menu-right');

    // Scroll 5
    var scroller5  = null;
    var scrollbar5 = null;

    scroller5  = new jsScroller(document.getElementById("Scroller-5"), 400, 200, "Scroller-Container-5");
    scrollbar5 = new jsScrollbar(document.getElementById("Scrollbar-Container-5"), scroller5, false);

    $(window).on('load resize', function() {
      scroller5  = new jsScroller(document.getElementById("Scroller-5"), 400, 200, "Scroller-Container-5");
      scrollbar5 = new jsScrollbar(document.getElementById("Scrollbar-Container-5"), scroller5, false);
    });
  });

  $('#field5').click(function () {
    literarische_werke.hide();
    literarische_projekte.hide();
    musikalische_werke.hide();
    musikalische_projekte.hide();
    musikinstrumentenbau.show();
    projekte_instrumentenbau.hide();
    $('#field1').css('font-size', '15px');
    $('#field2').css('font-size', '15px');
    $('#field3').css('font-size', '15px');
    $('#field4').css('font-size', '15px');
    $('#field5').css('font-size', '19px');
    $('#field6').css('font-size', '15px');
    $('#field1 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field2 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field3 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field4 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field5 span').addClass('glyphicon glyphicon-menu-right');
    $('#field6 span').removeClass('glyphicon glyphicon-menu-right');

    // Scroll 6
    var scroller6  = null;
    var scrollbar6 = null;

    scroller6  = new jsScroller(document.getElementById("Scroller-6"), 400, 200, "Scroller-Container-6");
    scrollbar6 = new jsScrollbar(document.getElementById("Scrollbar-Container-6"), scroller6, false);

    $(window).on('load resize', function() {
      scroller6  = new jsScroller(document.getElementById("Scroller-6"), 400, 200, "Scroller-Container-6");
      scrollbar6 = new jsScrollbar(document.getElementById("Scrollbar-Container-6"), scroller6, false);
    });
  });

  $('#field6').click(function () {
    literarische_werke.hide();
    literarische_projekte.hide();
    musikalische_werke.hide();
    musikalische_projekte.hide();
    musikinstrumentenbau.hide();
    projekte_instrumentenbau.show();
    $('#field1').css('font-size', '15px');
    $('#field2').css('font-size', '15px');
    $('#field3').css('font-size', '15px');
    $('#field4').css('font-size', '15px');
    $('#field5').css('font-size', '15px');
    $('#field6').css('font-size', '19px');
    $('#field1 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field2 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field3 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field4 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field5 span').removeClass('glyphicon glyphicon-menu-right');
    $('#field6 span').addClass('glyphicon glyphicon-menu-right');

    // Scroll 7
    var scroller7  = null;
    var scrollbar7 = null;

    scroller7  = new jsScroller(document.getElementById("Scroller-7"), 400, 200, "Scroller-Container-7");
    scrollbar7 = new jsScrollbar(document.getElementById("Scrollbar-Container-7"), scroller7, false);

    $(window).on('load resize', function() {
      scroller7  = new jsScroller(document.getElementById("Scroller-7"), 400, 200, "Scroller-Container-7");
      scrollbar7 = new jsScrollbar(document.getElementById("Scrollbar-Container-7"), scroller7, false);
    });
  });
  /* End - text-changer */

  /* Start - runSlide */
  runslide1();
  runslide2();

  function runslide1() {
    if ($("#slideshow1").children('img').length === 1) {
      return;
    }

    if ($("#slideshow1").children('img').length === 2) {
      $('#id-2').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#id-1').fadeIn(0).delay(3500).fadeOut(0, function() {
          runslide1();
        });
      });
    } else if ($("#slideshow1").children('img').length === 3) {
      $('#id-3').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#id-2').fadeIn(0).delay(3500).fadeOut(0, function() {
          $('#id-1').fadeIn(0).delay(3500).fadeOut(0, function() {
            runslide1();
          });
        });
      });
    } else if ($("#slideshow1").children('img').length === 4) {
      $('#id-4').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#id-3').fadeIn(0).delay(3500).fadeOut(0, function() {
          $('#id-2').fadeIn(0).delay(3500).fadeOut(0, function() {
            $('#id-1').fadeIn(0).delay(3500).fadeOut(0, function() {
              runslide1();
            });
          });
        });
      });
    } else if ($("#slideshow1").children('img').length === 5) {
      $('#id-5').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#id-4').fadeIn(0).delay(3500).fadeOut(0, function() {
          $('#id-3').fadeIn(0).delay(3500).fadeOut(0, function() {
            $('#id-2').fadeIn(0).delay(3500).fadeOut(0, function() {
              $('#id-1').fadeIn(0).delay(3500).fadeOut(0, function() {
                runslide1();
              });
            });
          });
        });
      });
    } else if ($("#slideshow1").children('img').length === 6) {
      $('#id-6').fadeIn(0).delay(3500).fadeOut(0, function () {
        $('#id-5').fadeIn(0).delay(3500).fadeOut(0, function () {
          $('#id-4').fadeIn(0).delay(3500).fadeOut(0, function () {
            $('#id-3').fadeIn(0).delay(3500).fadeOut(0, function () {
              $('#id-2').fadeIn(0).delay(3500).fadeOut(0, function () {
                $('#id-1').fadeIn(0).delay(3500).fadeOut(0, function () {
                  runslide1();
                });
              });
            });
          });
        });
      });
    }
  }

  function runslide2() {
    if ($("#slideshow2").children('img').length === 1) {
      return;
    }

    if ($("#slideshow2").children('img').length === 2) {
      $('#secondId-2').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#secondId-1').fadeIn(0).delay(3500).fadeOut(0, function() {
          runslide2();
        });
      });
    } else if ($("#slideshow2").children('img').length === 3) {
      $('#secondId-3').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#secondId-2').fadeIn(0).delay(3500).fadeOut(0, function() {
          $('#secondId-1').fadeIn(0).delay(3500).fadeOut(0, function() {
            runslide2();
          });
        });
      });
    } else if ($("#slideshow2").children('img').length === 4) {
      $('#secondId-4').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#secondId-3').fadeIn(0).delay(3500).fadeOut(0, function() {
          $('#secondId-2').fadeIn(0).delay(3500).fadeOut(0, function() {
            $('#secondId-1').fadeIn(0).delay(3500).fadeOut(0, function() {
              runslide2();
            });
          });
        });
      });
    } else if ($("#slideshow2").children('img').length === 5) {
      $('#secondId-5').fadeIn(0).delay(3500).fadeOut(0, function() {
        $('#secondId-4').fadeIn(0).delay(3500).fadeOut(0, function() {
          $('#secondId-3').fadeIn(0).delay(3500).fadeOut(0, function() {
            $('#secondId-2').fadeIn(0).delay(3500).fadeOut(0, function() {
              $('#secondId-1').fadeIn(0).delay(3500).fadeOut(0, function() {
                runslide2();
              });
            });
          });
        });
      });
    } else if ($("#slideshow2").children('img').length === 6) {
      $('#secondId-6').fadeIn(0).delay(3500).fadeOut(0, function () {
        $('#secondId-5').fadeIn(0).delay(3500).fadeOut(0, function () {
          $('#secondId-4').fadeIn(0).delay(3500).fadeOut(0, function () {
            $('#secondId-3').fadeIn(0).delay(3500).fadeOut(0, function () {
              $('#secondId-2').fadeIn(0).delay(3500).fadeOut(0, function () {
                $('#secondId-1').fadeIn(0).delay(3500).fadeOut(0, function () {
                  runslide2();
                });
              });
            });
          });
        });
      });
    }
  }
  /* End - runSlider */

  /* Start - show popup */
  $('.coverImage').click(function (event) {
    var id = event.target.id;

    $('.' + id + '').show();
    $('.blackout').show();

    $('body').css('overflow', 'hidden');
  });

  $('.blackout').click(function () {
    $('.blackout').hide();
    $('.coverOverlay').hide();
    $('body').css('overflow', 'unset');
  });

  $('.closeCover').click(function () {
    $('.coverOverlay').hide();
    $('.blackout').hide();
    $('body').css('overflow', 'unset');
  });
  /* End - show popup */

  // Scroll 1
  if (document.getElementById("Scroller-1") !== null) {
      var scroller  = null;
      var scrollbar = null;

      scroller  = new jsScroller(document.getElementById("Scroller-1"), 400, 200, "Scroller-Container");
      scrollbar = new jsScrollbar(document.getElementById("Scrollbar-Container"), scroller, false);

      $(window).on('load resize', function() {
          scroller  = new jsScroller(document.getElementById("Scroller-1"), 400, 200, "Scroller-Container");
          scrollbar = new jsScrollbar(document.getElementById("Scrollbar-Container"), scroller, false);
      });
  }

  $('#accordion .panel-collapse').on('shown.bs.collapse', function () {
    $(this).prev().find(".glyphicon").removeClass("glyphicon-triangle-top").addClass("glyphicon-triangle-bottom");
  });
  //The reverse of the above on hidden event:
  $('#accordion .panel-collapse').on('hidden.bs.collapse', function () {
    $(this).prev().find(".glyphicon").removeClass("glyphicon-triangle-bottom").addClass("glyphicon-triangle-top");
  });
});