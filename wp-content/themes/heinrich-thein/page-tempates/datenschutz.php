<?php /* Template Name: Datenschutz */ ?>

<?php get_header(); ?>

<div class="datenschutz" id="datenschutz">
	<h1>Datenschutz</h1>
    <div class="content">
	    <?php echo get_field("datenschutz_text"); ?>
    </div>
</div>

<?php get_footer(); ?>
