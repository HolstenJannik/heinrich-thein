<?php /* Template Name: Impressum */ ?>

<?php get_header(); ?>

<div class="impressum" id="impressum">
    <h1>Impressum</h1>
    <div class="imprintContent">
        <p>
            <?php echo get_field('impressum_text'); ?>
        </p>
    </div>
</div>

<?php get_footer(); ?>
