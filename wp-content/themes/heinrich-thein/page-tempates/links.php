<?php /* Template Name: Links */ ?>

<?php get_header(); ?>

<div class="links" id="links">
	<div class="containerBox">
		<h1>Thein<br />Blechblas-<br />instrumente</h1>
		<?php if (have_rows('link_group')): ?>
			<?php while (have_rows('link_group')): the_row();
				$linkArray = get_sub_field('link');
				$linkText  = $linkArray['title'];
				$linkUrl   = $linkArray['url'];
			?>
				<a target="_blank" href="<?php echo $linkUrl; ?>"><?php echo $linkText; ?></a>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
