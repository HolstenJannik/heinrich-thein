<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define('DB_NAME', 'heinrich_thein');

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define('DB_USER', 'root');

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define('DB_PASSWORD', 'root');

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define('DB_HOST', 'localhost');

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8mb4');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

define('WP_HOME','http://localhost/heinrich-thein');
define('WP_SITEURL','http://localhost/heinrich-thein');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=3G@?F0AUAlbgP+WH$cjUm@D?BwWiZ=f}B`3YF vT</vQ.e7WYasd;pEM;tGlx`]');
define('SECURE_AUTH_KEY',  '(HJ` ?0t4|{=F|/hc9lt^*v hg5;:C2i*TeD5DJAD<GBLS(q]$tg1Oa2QVy7_j@5');
define('LOGGED_IN_KEY',    '&yVXU86%)T&.+xu$~g8r5;/m3Tht*h??#*UpFRMN? 7JM^y8cQW7d1)E^NPIF~%^');
define('NONCE_KEY',        'd+#p9yiiJ73ia/N#*@VTAgAVrmU|FOHNCtDY7f%FYjRTGc7Zst__T>33O$QZMbG}');
define('AUTH_SALT',        '-~7{OV@rk(Sy38 WmV]y$F4N$8BE{MaM8KVG~)U#t{ nbP,[ _^t*Gmd{jf}/V1@');
define('SECURE_AUTH_SALT', '#C<KUDjPzc9ngu;EZN7]I:<g>/sppU_4^w>{p7M9T@jx+mq<X?dx<?gxrdhV1XRH');
define('LOGGED_IN_SALT',   'T%t]fq_C{aams,IQn@H>B[zI6<qWCrWBy@Bze<u_PZ^TRP/[F*OfXP3wDU1< mru');
define('NONCE_SALT',       'N4PPR~M[!=tbD$)Nm;(WEl,lR{Jb@jA5e}g9ft-0j, C]K3z[9acJ8`FBQW>:.Vx');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once(ABSPATH . 'wp-settings.php');
