# Require any additional compass plugins here.
# require "susy"
# require "sass-globbing"
# require "breakpoint"

#Folder settings
project_type = :stand_alone
http_path = "/"
relative_assets = true      #because we're not working from the root
css_dir = "wp-content/themes/heinrich-thein/stylesheets/css"          #where the CSS will saved
sass_dir = "wp-content/themes/heinrich-thein/stylesheets/scss"           #where our .scss files are
#images_dir = "assets/img"    #the folder with your images
#javascripts_dir = "assets/javascript"

# You can select your preferred output style here (can be overridden via the command line):
output_style = :compressed

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

# Obviously
preferred_syntax = :scss

# Sourcemaps for Chrome DevTools

sass_options = {:sourcemap => false}
# sass_options = {:debug_info => true}
sourcemap = false